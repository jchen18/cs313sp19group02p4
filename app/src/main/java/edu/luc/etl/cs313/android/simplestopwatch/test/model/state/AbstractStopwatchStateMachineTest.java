package edu.luc.etl.cs313.android.simplestopwatch.test.model.state;

import android.widget.TextView;

import static edu.luc.etl.cs313.android.simplestopwatch.common.Constants.SEC_PER_MIN;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertFalse;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import edu.luc.etl.cs313.android.simplestopwatch.R;
import edu.luc.etl.cs313.android.simplestopwatch.android.StopwatchAdapter;
import edu.luc.etl.cs313.android.simplestopwatch.common.StopwatchUIUpdateListener;
import edu.luc.etl.cs313.android.simplestopwatch.model.clock.ClockModel;
import edu.luc.etl.cs313.android.simplestopwatch.model.clock.OnTickListener;
import edu.luc.etl.cs313.android.simplestopwatch.model.state.StopwatchStateMachine;
import edu.luc.etl.cs313.android.simplestopwatch.model.time.TimeModel;

/**
 * Testcase superclass for the stopwatch state machine model. Unit-tests the state
 * machine in fast-forward mode by directly triggering successive tick events
 * without the presence of a pseudo-real-time clock. Uses a single unified mock
 * object for all dependencies of the state machine model.
 *
 * @author laufer
 * //@see http://xunitpatterns.com/Testcase%20Superclass.html
 *
 */
public abstract class AbstractStopwatchStateMachineTest {

    private StopwatchStateMachine model;

    private UnifiedMockDependency dependency;

    @Before
    public void setUp() throws Exception {
        dependency = new UnifiedMockDependency();
    }

    @After
    public void tearDown() {
        dependency = null;
    }

    /**
     * Setter for dependency injection. Usually invoked by concrete testcase
     * subclass.
     *
     * @param model
     */
    protected void setModel(final StopwatchStateMachine model) {
        this.model = model;
        if (model == null)
            return;
        this.model.setUIUpdateListener(dependency);
        this.model.actionInit();
    }

    protected UnifiedMockDependency getDependency() {
        return dependency;
    }

    /**
     * Verifies that we're initially in the stopped state (and told the listener
     * about it).
     */
    @Test
    public void testPreconditions() {
        assertEquals(R.string.STOPPED, dependency.getState());
    }

    /**
     * Verifies the following scenario: time is 0, press start, wait 5+ seconds,
     * expect time 5.
     */
    @Test
    public void testScenarioRun() {
        assertTimeEquals(5);
        // directly invoke the button press event handler methods
        model.onStartStop();
        onTickRepeat(5);
        assertTimeEquals(0);
    }

    /**
     * Verifies the following scenario: time begins at 0 secs, time is incremented to 5 secs, time is paused, timer should reset to 0
     */
    @Test
    public void testTimeReset(){
        assertTimeEquals(0);
        model.onStartStop();
        onTickRepeat(5);
        model.onStartStop();
        assertTimeEquals(0);
    }

    /**
     * Verifies the following scenario: time begins at 0 secs, time is to 99 secs, timer should immediately countdown
     */
    @Test
    public void testImmediateCountdown(){
        assertTimeEquals(0);
        model.onStartStop();
        onTickRepeat(99);
        assertTimeEquals(0);
    }

    /**
     * Verifies the following scenario:  time begins at 0 secs, incremented to 10 for test purposes, and waits 3 seconds before
     * transitioning to running state.
     */
    @Test
    public void testThreeSecondCountdown() throws InterruptedException{
        assertTimeEquals(0);
        Thread.sleep(3000);
        model.onStartStop();
        onTickRepeat(10);
        assertTimeEquals(0);

    }

    /**
     * Sends the given number of tick events to the model.
     *
     *  @param n the number of tick events
     */
    protected void onTickRepeat(final int n) {
        for (int i = 0; i < n; i++)
            model.onTick();
    }

    /**
     * Checks whether the model has invoked the expected time-keeping
     * methods on the mock object.
     */
    protected void assertTimeEquals(final int t) {
        assertEquals(t, dependency.getTime());
    }
}

/**
 * Manually implemented mock object that unifies the three dependencies of the
 * stopwatch state machine model. The three dependencies correspond to the three
 * interfaces this mock object implements.
 *
 * @author laufer
 * //@see http://xunitpatterns.com/Testcase%20Superclass.html
 */
class UnifiedMockDependency implements TimeModel, ClockModel, StopwatchUIUpdateListener {

    private int timeValue = -1, stateId = -1;

    private int runningTime = 0;

    private boolean started = false;

    private int incTime = 0;

    public int getTime() {
        return timeValue;
    }

    public int getState() {
        return stateId;
    }

    public boolean isStarted() {
        return started;
    }

    @Override
    public void updateTime(final int timeValue) {
        this.timeValue = timeValue;
    }

    @Override
    public void updateState(final int stateId) {
        this.stateId = stateId;
    }

    //@Override
    //public void updateEditable(final int statId) { this.stateId = stateId; }

    @Override
    public void setOnTickListener(OnTickListener listener) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void start() {
        started = true;
    }

    @Override
    public void stop() {
        started = false;
    }

    @Override
    public void resetRuntime() {
        runningTime = 0;
    }

    @Override
    public void incRuntime() {
        runningTime++;
    }

    @Override
    public int getRuntime() {
        return runningTime;
    }

    @Override
    public void setIncTime() { incTime++;};

    @Override
    public int getIncTime() { return incTime;}

    @Override
    public void decIncTime(){incTime--;}
}
