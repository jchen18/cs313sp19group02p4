package edu.luc.etl.cs313.android.simplestopwatch.model.state;

import android.widget.TextView;

import edu.luc.etl.cs313.android.simplestopwatch.common.StopwatchUIListener;
import edu.luc.etl.cs313.android.simplestopwatch.model.clock.OnTickListener;

/**
 * A state in a state machine. This interface is part of the State pattern.
 *
 * @author laufer
 */
interface StopwatchState extends StopwatchUIListener, OnTickListener/*, TextView.OnEditorActionListener*/ {
    void updateView();
    int getId();
}
