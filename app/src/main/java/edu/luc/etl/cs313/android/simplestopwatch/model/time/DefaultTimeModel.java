package edu.luc.etl.cs313.android.simplestopwatch.model.time;

import static edu.luc.etl.cs313.android.simplestopwatch.common.Constants.*;

/**
 * An implementation of the stopwatch data model.
 */
public class DefaultTimeModel implements TimeModel {

    private int runningTime = 0;
    private int incTime =0;


    @Override
    public void resetRuntime() {
        runningTime = 0;
    }

    @Override
    public void incRuntime() {
        runningTime = (runningTime + SEC_PER_TICK) % SEC_PER_HOUR;
    }

    @Override
    public int getRuntime() {
        return runningTime;
    }

   @Override
   public void setIncTime() {
       if(incTime<99){
           incTime += SEC_PER_TICK;
       }
   }
   @Override
    public int getIncTime(){
        return incTime;
   }
   @Override
    public void decIncTime(){
        incTime = ((incTime - SEC_PER_TICK) % SEC_PER_HOUR);
    }

}