package edu.luc.etl.cs313.android.simplestopwatch.model.state;

import edu.luc.etl.cs313.android.simplestopwatch.R;

import edu.luc.etl.cs313.android.simplestopwatch.android.StopwatchAdapter;

/**
 * A new state that increments the stopwatch and implements the interface called Stopwatch State.
 */
class IncrementState extends StopwatchAdapter implements StopwatchState{

    private final StopwatchSMStateView sm;
    private int currentTime;

    public IncrementState(final StopwatchSMStateView sm) {
        this.sm = sm;
    }

    //gets the current time to store so it can increment it by 1 and then continues that process until button is not pressed
    @Override
    public void onStartStop(){
        currentTime= sm.actionGetCurrentTime();
        sm.actionIncTime();
        sm.actionUpdateView();
        sm.toIncrementState();
    }
    //as its incrementing if it waits three seconds it starts decrementing and goes to running state
    //otherwise, if 3 seconds doesn't pass it continues to increment
    @Override
    public void onTick() {
        sm.actionInc();
        if(sm.actionGetCurrentTime() - currentTime == 3){
            sm.actionDecTime();
            sm.toRunningState();
        }else {
            sm.toIncrementState();
        }
    }


    /* @Override
     public void onEditorAction() {
      throw new UnsupportedOperationException("onEditorAction");
     }*/


    @Override
    public void updateView() {
        sm.updateUIRuntime();
    }

    @Override
    public int getId() {
        return R.string.INCREMENT;
    }
}
