package edu.luc.etl.cs313.android.simplestopwatch.model.state;

import edu.luc.etl.cs313.android.simplestopwatch.R;

class RunningState implements StopwatchState {

    public RunningState(final StopwatchSMStateView sm) {
        this.sm = sm;
    }

    private final StopwatchSMStateView sm;

    @Override
    public void onStartStop() {
        sm.actionStop();
        sm.actionReset();
        int time = sm.actionGetIncTime();
        for (int i = time; i > 0; i--) {
            sm.actionDecTime();
        }
        sm.toStoppedState();
    }

    @Override
    public void onTick() {
        sm.actionInc();
        sm.actionDecTime();
        if ( sm.actionGetIncTime() > 0 ){ sm.toRunningState(); }
        else { sm.updateUIRuntime(); sm.toAlarmState(); }
    }

    /*
     *@Override
     *public void onEditorAction() {
     * throw new UnsupportedOperationException("onEditorAction");
     *}
     */

    @Override
    public void updateView() {
        sm.updateUIRuntime();
    }

    @Override
    public int getId() {
        return R.string.RUNNING;
    }
}
