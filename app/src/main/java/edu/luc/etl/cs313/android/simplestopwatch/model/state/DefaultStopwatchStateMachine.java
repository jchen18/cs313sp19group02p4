package edu.luc.etl.cs313.android.simplestopwatch.model.state;

import edu.luc.etl.cs313.android.simplestopwatch.common.StopwatchUIUpdateListener;
import edu.luc.etl.cs313.android.simplestopwatch.model.clock.ClockModel;
import edu.luc.etl.cs313.android.simplestopwatch.model.time.TimeModel;

/**
 * An implementation of the state machine for the stopwatch.
 *
 * @author laufer
 */
public class DefaultStopwatchStateMachine implements StopwatchStateMachine {

    public DefaultStopwatchStateMachine(final TimeModel timeModel, final ClockModel clockModel) {
        this.timeModel = timeModel;
        this.clockModel = clockModel;
    }

    private final TimeModel timeModel;

    private final ClockModel clockModel;

    /**
     * The internal state of this adapter component. Required for the State pattern.
     */
    private StopwatchState state;

    protected void setState(final StopwatchState state) {
        this.state = state;
        uiUpdateListener.updateState(state.getId());
        //uiUpdateListener.updateEditable(state.getId());
    }

    private StopwatchUIUpdateListener uiUpdateListener;

    @Override
    public void setUIUpdateListener(final StopwatchUIUpdateListener uiUpdateListener) {
        this.uiUpdateListener = uiUpdateListener;
    }

    // forward event uiUpdateListener methods to the current state
    // these must be synchronized because events can come from the
    // UI thread or the timer thread
    @Override public synchronized void onStartStop() { state.onStartStop(); }
    @Override public synchronized void onTick()      { state.onTick(); }
    //@Override public synchronized void onEditorActionListener() { state.onEditorAction(null, 6, null);}

    @Override public void updateUIRuntime() { uiUpdateListener.updateTime(timeModel.getIncTime()); }


    // known states
    private final StopwatchState STOPPED     = new StoppedState(this);
    private final StopwatchState RUNNING     = new RunningState(this);
    // new states we created
    private final StopwatchState INCREMENT   = new IncrementState(this);
    private final StopwatchState ALARM       = new AlarmState( this );

    // transitions
    @Override public void toRunningState()    { setState(RUNNING); }
    @Override public void toStoppedState() { setState(STOPPED); }
    // new transitions to the states we created not in the example
    @Override public void toIncrementState()  { setState(INCREMENT); }
    @Override public void toAlarmState() {setState(ALARM);}


    // actions that came with example
    @Override public void actionInit()       { toStoppedState(); actionReset(); }
    @Override public void actionReset()      { timeModel.resetRuntime(); actionUpdateView(); }
    @Override public void actionStart()      { clockModel.start(); }
    @Override public void actionStop()       { clockModel.stop(); }
    @Override public void actionInc()        { timeModel.incRuntime(); actionUpdateView(); }
    // new actions we created for increment state
    @Override public int actionGetCurrentTime() {return timeModel.getRuntime();}
    @Override public void actionIncTime() { timeModel.setIncTime(); actionUpdateView();}
    @Override public void actionDecTime() { timeModel.decIncTime();
        //if (actionGetCurrentTime() > 0) timeModel.decIncTime();
        actionUpdateView();
    }
    @Override public int actionGetIncTime() {return timeModel.getIncTime();}
    @Override public void actionUpdateView() { state.updateView(); }
}
