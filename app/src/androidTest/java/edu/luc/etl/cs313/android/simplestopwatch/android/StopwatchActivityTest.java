package edu.luc.etl.cs313.android.simplestopwatch.android;

import android.support.test.filters.SmallTest;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import org.junit.Rule;
import org.junit.runner.RunWith;

import edu.luc.etl.cs313.android.simplestopwatch.test.android.AbstractStopwatchActivityTest;import edu.luc.etl.cs313.android.simplestopwatch.android.StopwatchAdapter;
@RunWith(AndroidJUnit4.class)
@SmallTest
/**
 * Concrete Android test subclass. Has to inherit from framework class
 * and uses delegation to concrete subclass of abstract test superclass.
 * IMPORTANT: project must export JUnit 4 to make it available on the
 * device.
 *
 * @author laufer
 * @see "https://developer.android.com/training/testing/ui-testing/"
 */
public class StopwatchActivityTest extends AbstractStopwatchActivityTest {

    @Rule
    public ActivityTestRule<StopwatchAdapter> activityRule =
            new ActivityTestRule<>(StopwatchAdapter.class);

    @Override
    protected StopwatchAdapter getActivity() {
        return activityRule.getActivity();
    }


    private static StopwatchAdapter stopwatchAdapter = null;
    private AbstractStopwatchActivityTest actualTest;

    public void testActivityCheckTestCaseSetUpProperly() {
        actualTest.testActivityCheckTestCaseSetUpProperly();
    }

    public void testActivityScenarioRun() throws Throwable {
        actualTest.testActivityScenarioRun();
    }

    /*public void testActivityScenarioRunLapReset() throws Throwable {
        actualTest.testActivityScenarioRunLapReset();
    }*/
}
