package edu.luc.etl.cs313.android.simplestopwatch.model.state;

import edu.luc.etl.cs313.android.simplestopwatch.R;

/**
 * A new state that sounds the alarm and implements the interface called Stopwatch State.
 */
class AlarmState implements StopwatchState{


    public AlarmState(final StopwatchSMStateView sm) {
        this.sm = sm;
    }

    private final StopwatchSMStateView sm;
    //goes to stopped state after stopping timer and resetting time to 0
    @Override
    public void onStartStop() {
        sm.actionStop();
        sm.actionReset();
        sm.toStoppedState();
    }
    //decrements timer
    @Override
    public void onTick() {
        //if (sm.actionGetIncTime() == -1) {
           // sm.actionIncTime();
        //}
        sm.toAlarmState();
    }


     /*@Override
     public void onEditorAction() {
      throw new UnsupportedOperationException("onEditorAction");
     }*/


    @Override
    public void updateView() {
        sm.updateUIRuntime();
    }

    @Override
    public int getId() {
        return R.string.ALARM;
    }
}